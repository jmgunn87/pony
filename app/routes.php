<?php

$app->before (function () use ($app) {
	$app->locale = 'en_GB';
	$app->templating->setGlobal('app', $app);
});

$app->get ('/', function () use ($app) {
	$app->copy = $app->copyLoader->load('default.php');
	echo $app->templating->render ("index.html.php");
});
