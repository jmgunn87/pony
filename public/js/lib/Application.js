function Application () {
    var args = Array.prototype.slice.call (arguments);
    var callback = args.pop ();
    if (!(this instanceof Application)) {
        return new Application (calllback);
    }    
    for (i in Application.modules) {
        Application.modules[i](this);
    }
    callback(this);
}
Application.prototype.version = '1.0';
Application.modules = {};