Application.modules.Observer = function (app) {
    app.subscribers = app.subscribers || {};
    app.subscribe = function (context, event, callback) {
        var subscription = {context: context, callback: callback};
        event in this.subscribers ? 
            this.subscribers[event].push(subscription) : 
            this.subscribers[event] = [subscription];
    };
    app.unsubscribe = function (context, event) {
        if (event in this.subscribers) {
            for (var i = this.subscribers[event].length; --i > -1;) {
                if (this.subscribers[event].context === context) {
                    delete this.subscribers[event];
                }
            }
        }
    };
    app.publish = function (event) {
        if (event in this.subscribers) {
            for (var i = this.subscribers[event].length; --i > -1;) {
                this.subscribers[event][i].callback.apply(
                    this.subscribers[event][i].context, 
                    Array.prototype.slice.call(arguments, 1));
            }
        }
    };
};
