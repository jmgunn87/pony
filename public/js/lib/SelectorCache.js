Application.modules.$$ = function (app) {
  app.$$ = function (selector) {
	app.$$.cache = app.$$.cache || {};
	return app.$$.cache [selector] =
	  app.$$.cache [selector] || $(selector);
  };
};
