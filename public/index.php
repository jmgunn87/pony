<?php

include (__DIR__.'/../lib/Context.php');
include (__DIR__.'/../lib/AppService.php');
include (__DIR__.'/../lib/HttpRouter.php');
include (__DIR__.'/../lib/HttpApplication.php');
include (__DIR__.'/../lib/services/TemplateEngine.php');
include (__DIR__.'/../lib/services/CopyLoader.php');
include (__DIR__.'/../lib/vendor/RedBeanPHP3_3_3/rb.php');

$app = new HttpApplication ();
$app->loadConfigs (__DIR__.'/../config');

R::setup ("mysql:host={$app->dbhost};dbname={$app->dbname}", $app->dbuser, $app->dbpass);
if ($app->env === 'production') {
	R::freeze (true);
}

$app->templating = new TemplateEngine ($app);
$app->copyLoader = new CopyLoader ($app);

include (__DIR__.'/../app/routes.php');
$app->run ();
exit;
