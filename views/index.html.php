<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <!--[if lt IE 7]>
        <p class="chromeframe">You are using an outdated browser. 
        <a href="http://browsehappy.com/">Upgrade your browser today</a> or 
        <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> 
        to better experience this site.</p>
        <![endif]-->
        <div id="container">
        	<h1><?php echo $app->copy['title']; ?></h1>
			<p><?php echo $app->copy['description']; ?></p>
        </div>
        <script src="js/vendor/jquery-1.8.1.min.js"></script>
        <script src="js/lib/Application.js"></script>
        <script src="js/lib/SelectorCache.js"></script>
        <script src="js/lib/Observer.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
