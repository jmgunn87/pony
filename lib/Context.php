<?php
/**
 * @author James Marshall-Gunn <james.gunn@fanatix.com>
 */
class Context
{
	protected $register = array ();
	
	function __set ($key, $class) {
		$this->register[$key] = $class;
	}
	
	function __get ($key) {
		return $this->register[$key];
	}
}