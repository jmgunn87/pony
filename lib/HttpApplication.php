<?php
/**
 * @author James Marshall-Gunn <james.gunn@fanatix.com>
 */
class HttpApplication extends Context
{
	protected $beforeAction;
	protected $afterAction;
	
	public function __construct () {
		$this->router = new HttpRouter ();
	}
	
	public function getLocaleAwareFile ($dir, $file, $locale = null) {
		if (!$locale) {
			$locale = $this->locale;
		}
		
		$localeFile = "$dir/{$locale}/$file";
		if ($locale && file_exists ($localeFile)) {
			return $localeFile;
		} else {
			return "$dir/$file";
		}
	}
	
	public function loadConfigs ($configsDir) {
		$app = $this;
		include ($configsDir. '/default.inc');
		$app->env = getenv ('APPLICATION_ENV');
		$configFile = $configsDir. "/{$app->env}.inc";
		if (file_exists ($configFile)) {
			include ($configFile);
		}
	}
	
	public function before ($action) {
		$this->beforeAction = $action;
	}
	
	public function __call($method, $args) {
		$this->router->addPath ($method, $args[0], $args[1]);
	}

	public function after ($action) {
		$this->afterAction = $action;
	}
	
	public function run () {
		$filter = FILTER_SANITIZE_STRING;
		$this->request = new stdClass();
		$this->request->server = isset ($_SERVER) ? 
			filter_var_array ($_SERVER, $filter) : array ();
		$this->request->post = isset($_POST) ? 
			filter_var_array ($_POST, FILTER_SANITIZE_STRING) : array ();
		$this->request->get = isset($_GET) ? 
			filter_var_array ($_GET, FILTER_SANITIZE_STRING) : array ();
		if ($this->env != 'production') {
			header('Cache-Control: no-cache');
			header('Pragma: no-cache');
		}
		
		if ($this->beforeAction) {
			$action = $this->beforeAction;
			$action();
		}
		
		$route = $_GET ['_route'];
		$this->router->matchPath (strtolower ($_SERVER['REQUEST_METHOD']), $route);
		
		if ($this->afterAction) {
			$action = $this->afterAction();
			$action();
		}
	}
}