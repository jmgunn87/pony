<?php
/**
 * @author James Marshall-Gunn <james.gunn@fanatix.com>
 */
class TemplateEngine extends AppService
{
	protected $globals = array ();
	
	public function setGlobal ($key, $value) {
		$this->globals[$key] = $value;
	}
	
	public function render ($template, $args = array ()) {
		foreach ($this->globals as $key => $value) {
			$args [$key] = $value;
		}
		extract ($args, EXTR_OVERWRITE);
		ob_start ();
		include ($this->app->getLocaleAwareFile(
			$this->app->templateDir, $template));
		$output = ob_get_contents ();
		ob_end_clean ();
		return $output;
	}
}