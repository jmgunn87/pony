<?php

class CopyLoader extends AppService 
{	
	public function load ($file, $locale = null) {
		return include ($this->app->getLocaleAwareFile($this->app->copyDir, $file));
	}
}