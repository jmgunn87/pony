<?php

/**
 * @author James Marshall-Gunn <james.gunn@fanatix.com>
 */
class HttpRouter
{
	protected $pathMap = array ();
	 
	public function addPath ($method, $route, $action) {
		if (!isset ($this->pathMap [strtolower ($method)])) {
			$this->pathMap [strtolower ($method)] = array(
				'children' => array ()
			);
		}
		
		$currentLevel = &$this->pathMap [strtolower ($method)]['children'];
		foreach (explode ('/', $route) as $part) {
			if (!$part) {
				continue;
			}
			$part = strchr ($part, ':') ? ':' : $part;	//recognise a variable
			if (!isset ($currentLevel [$part])) {
				$currentLevel [$part] = array (
					'action'   => null, 'children' => array ()
				);
			}
			$currentLevel = &$currentLevel [$part]['children'];
		}
		$currentLevel['action'] = $action;
	}
	
	public function matchPath ($method, $route) {
		if (!isset ($this->pathMap [strtolower ($method)])) {
			throw new Exception('bad method');
		}
		
		$currentLevel = &$this->pathMap [strtolower ($method)]['children'];
		$arguments = array ();
		foreach (explode ('/', $route) as $part) {
			if (isset ($currentLevel [$part])) {
				$currentLevel = &$currentLevel [$part]['children'];
			} else if (isset ($currentLevel [':'])){
				$arguments []= $part;
				$currentLevel = &$currentLevel [':']['children'];
			}
		}
		
		if (isset ($currentLevel ['action'])) {
			if (is_callable ($currentLevel ['action'])) {
				return call_user_func_array ($currentLevel ['action'], $arguments);
			}
		} else {
			throw new Exception ('unable to resolve route "' . $route . '"', 404);
		}
	}
}